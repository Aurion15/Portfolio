function global(){
     $(".specialChar").each(function() {
            var html = $(this).html().split(" ");
            var newhtml = [];
            for(var i=0; i< html.length; i++) {
               if(i>0 && (i%1) == 0)
                  newhtml.push("</span><span>");
               newhtml.push(html[i]);
            }

            $(this).html("<span class='split-char'>"+ newhtml.join(" ") +"</span>");
         });
}

function tab(){
     $( "#tabs" ).tabs();
}

function slidehome(){
            $('.sliderhome').flexslider({
              animation: "fade",controlNav: false,directionNav:false
            });

            $('.flexslider1').flexslider({
              animation: "slide",
              controlNav: false,
              animationLoop: false,
              itemWidth: 210,
              itemMargin: 5,
              minItems: 3,
              maxItems:3
            });

             $('.flexslider2').flexslider({
              animation: "fade",controlNav:true,directionNav:false, 
            });
             $('#carousel').flexslider({
              animation: "slide",
              controlNav: false,
              animationLoop: false,
              slideshow: false,
              itemWidth: 210,
              itemMargin: 5,
              asNavFor: '#slider'
            });
           
            $('#slider').flexslider({
              animation: "slide",
              controlNav: false,
              animationLoop: false,
              slideshow: false,
              sync: "#carousel"
            });
}

function scroll(){
     $(".botmiddle").mCustomScrollbar({
                     scrollInertia: 400
                  });
}

function menu(){
     $('#menu').mmenu({
          offCanvas: {
              position  : "right",
              zposition : "front"
            },
          navbars   : [
            {
              position  : 'top',
              content   : [
                'prev',
                'title',
                'close'
              ]
            }, {
              position  : 'bottom',
              content   : [
                '<p>Follow us</p><a href="#" class="fb"></a><a href="#" class="yt"></a><a href="#" class="in"></a>'
              ]
            }
          ]
      })
}