<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Galery_model extends CI_Model {

		function __construct(){
			parent::__construct();
			$this->load->database();
		}

		function uploadPicture($pictureData){
			$this->db->insert('galery',$pictureData);
		}

		function viewGalery(){
			$view = $this->db->get('galery');
			return $view->result();
		}

		function latestGalery(){
			$view = $this->db->query('select * from galery order by id DESC limit 5');
			return $view->result();
		}

		function deletePicture($id){
			$this->db->delete('galery',array('id'=>$id));
		}

		public function getPicture($id){
			$Picture = $this->db->get_where('galery',array('id' =>$id));
			return $Picture->result();
		}

		public function countRow(){
			$count = $this->db->query('SELECT COUNT(id) AS COUNT FROM galery');
			return $count->result();
		}

		function updateGalery($data,$id){
			$this->db->where('id', $id);
			$this->db->update('galery',$data);
		}

	}

?>