<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller {

	public function Category(){
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->model('Category_model');
		$this->load->library('session');
	}

	public function View(){
		if($this->session->userdata('log')){
			$data['Category'] = $this->Category_model->ViewCategory();
			$data['Edit'] = false;
			$data['message'] ='';
			$this->load->view('admin/Category', $data);
		}else{
			redirect('MahkotaDirfan');
		}
	}

	public function submitCategory(){
		$config['upload_path']   = './pictures/category/';
        $config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']      = 4000;
		$config['remove_spaces'] = FALSE;

		$config['file_name'] = $this->input->post('type');

		$this->load->library('upload', $config);
		 if ( ! $this->upload->do_upload('file')) {
		    $data['Category'] = $this->Category_model->ViewCategory();
			$data['Edit'] = false;
			$data['message'] ='please select a picture';
			$this->load->view('admin/Category', $data);
		 }
		 else {
		 	$img_data=$this->upload->data();
            $new_imgname=$config['file_name'].$img_data['file_ext'];

			$categoryData = array(
				'name' => $this->input->post('type'),
				'picture' => $new_imgname
			);

			$this->Category_model->addCategory($categoryData);

	   		redirect('Category/View');
		 }
	}

	public function deleteCategory($id){
		if($this->session->userdata('log')){
			if(is_null($id)){
				redirect('/404/');
			}else{
				$this->Category_model->deleteCategory($id);
				redirect('Category/View');
			}
		}else{
			redirect('MahkotaDirfan');
		}
	}

	public function updateCategory($id){
		if($this->session->userdata('log')){
			$data = array(
	            'name'=> $this->input->post('type'),
	        );
			$this->Category_model->updateCategory($data, $id);
			$this->View();
		}else{
			redirect('MahkotaDirfan');
		}
	}

	public function ViewUpdate($id){
		if($this->session->userdata('log')){
			$data['Category'] = $this->Category_model->ViewCategory();
			$data['Edit'] = true;
			$data['id'] = $id;
			$data['Update'] = $this->Category_model->getCategory($id);
			$this->load->view('admin/Category', $data);
		}else{
			redirect('MahkotaDirfan');
		}
	}
}
?>