<!doctype html>
<html class="no-js" lang="en">
  <head>
    <!-- Header  -->
    <?php $this->load->view('front/Headlib'); ?>
  </head>
  <body>
    <!-- Header  -->
    <?php $this->load->view('front/Header'); ?>
    
    <div class="sparatorser">
      <div class="caption columns centered">
        <h1>LATEST <span>NEWS</span></h1>
        <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;/  News</p>
      </div>
    </div>
    <div class="container no-padding">
      <div class="row">
        <div class="heading">
          <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;News
        </div>
        
        <div class="news">
            <?php
              foreach ($Latest as $latest) 
                $originalDate = $latest->date;
                $newDate = date("d-M-Y", strtotime($originalDate));{?>
                <div class="headline large-12 columns no-padding">
                   <div class="headlineleft large-6 columns no-padding">
                       <img src="<?php echo base_url();?>pictures/news/<?php echo $latest->picture?>" alt="">
                   </div>
                   <div class="headlineright large-6 columns no-padding">
                       <h4 class="date">
                           <?php echo $newDate ?>
                       </h4>
                      <h1 class="title"><?php echo $latest->title?></h1>
                      <p>
                          <?php echo $latest->note?>
                      </p>
                      <h5><a href="<?php echo base_url().'News/Detail/'.$latest->id?>">Read More</a></h5><h5>
                   </h5></div>
                </div>
            <?php  }
            ?>
            
            <div class="newstools large-12 columns">
                <h2 class="caption">
                    Sort by
                </h2>
                 <select class="selectsearch" name="" id="">
                    <option value="">Latest</option>
                    <option value="">Oldest</option>
                </select>

                <form>
                    <input type="text" placeholder="Search..." required="">
                    <button><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
            <div class="newstop large-12 columns no-padding"> 
                <ul class="large-block-grid-3">
                    <?php 
                      foreach ($News as $news) { 
                      $originalDate = $latest->date;
                      $newDate = date("d-M-Y", strtotime($originalDate));?>
                      <li>
                              <img src="<?php echo base_url();?>pictures/news/<?php echo $news->picture?>" alt="">
                              <div class="listnewscap">
                                  <span><?php echo $newDate ?></span>
                                  <h1>
                                      <?php echo $news->title?>
                                  </h1>
                                  <h2>
                                      <a href="<?php echo base_url().'News/Detail/'.$latest->id?>">Read More</a>
                                  </h2>
                              </div>
                      </li>
                    <?php  }
                    ?>
                </ul>
            </div>
            <div class="buttonlist small-2 columns small-centered no-padding">
              <a href="">Load More</a>
            </div>
        </div>
                    <!-- End News  -->
      </div>
    </div>
    <div class="medpartner no-padding">
      <center><img src="<?php echo base_url();?>assets/img/sponsored.jpg" class="centered" alt=""></center>
    </div>
    
    <?php $this->load->view('front/Footer'); ?>
    
    <?php $this->load->view('front/Footlib'); ?>
     
  </body>
</html>
