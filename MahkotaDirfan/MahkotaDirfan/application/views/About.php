<!doctype html>
<html class="no-js" lang="en">
  <head>
    <!-- Header  -->
    <?php $this->load->view('front/Headlib'); ?>
  </head>
  <body>
    <!-- Header  -->
    <?php $this->load->view('front/Header'); ?>
    
    <div class="container no-padding">
      <div class="row">
        <div class="heading">
          <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;About PT.Mahkota Dirfan Berkah
        </div>
        <div class="aboutus">
          <div class="large-12 columns no-padding">
            <div class="large-6 columns no-padding">
              <p>PT.Mahkota Dirfan Berkah adalah perusahaan distributor alat-alat kesehatan seperti disposible equipment, diagnostic tools, medical equiSpment dan alat-alat laboratorium lainya. kami telah melayani ratusan rumah sakit dan instansi kesehatan di Indonesia. Kualitas, kecepatan , komunikasi dan juga kepuasan pelanggan menjadi prioritas kami.</p>
              <h2>Our History</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum do
              </p><br/>
              <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum do<p>
            </div>
            <div class="large-6 columns no-padding">
              <img src="<?php echo base_url();?>assets/img/content/Layer-123.jpg" alt="">
            </div>
          </div>
          <div class="ourcommitment large-12 columns no-padding">
            <div class="visi large-4 columns">
             <h1>Misi</h1>
             <ul>
               <li>Menjadi yang terbaik di Indonesia</li>
               <li>Barang Lengkap</li>
               <li>Kepuasan anda merupakan prioritas kami</li>
               <li>Kepuasan anda merupakan prioritas kami</li>
             </ul>
            </div>
            <div class="misi large-4 columns">
              <h1>Misi</h1> 
              <ul>
               <li>Menjadi yang terbaik di Indonesia</li>
               <li>Barang Lengkap</li>
               <li>Kepuasan anda merupakan prioritas kami</li>
               <li>Kepuasan anda merupakan prioritas kami</li>
             </ul>
            </div>
            <div class="moto large-4 columns">
              <h1>Moto</h1>
              <ul>
               <li>Menjadi yang terbaik di Indonesia</li>
               <li>Barang Lengkap</li>
               <li>Kepuasan anda merupakan prioritas kami</li>
               <li>Kepuasan anda merupakan prioritas kami</li>
             </ul>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    <div class="medpartner no-padding">
      <center><img src="<?php echo base_url();?>assets/img/sponsored.jpg" class="centered" alt=""></center>
    </div>
    <div class="footer">
      <div class="row">
        <div class="small-12 columns no-padding">
          <div class="small-3 columns no-padding">
            <h1>Jam Operasional </h1>
           <table class="tg">
              <tr>
                <th class="tg-yw4l">Senin-Jumat</th>
                <th class="tg-yw4l">     -     </th>
                <th class="tg-yw4l">08:30 - 16:30</th>
              </tr>
              <tr>
                <td class="tg-yw4l">Sabtu</td>
                <td class="tg-yw4l">     -     </td>
                <td class="tg-yw4l">08.30 - 13:30</td>
              </tr>
              <tr>
                <td class="tg-yw4l">Minggu /  Hari libur </td>
                <td class="tg-yw4l">     -     </td>
                <td class="tg-ljhg">Close</td>
              </tr>
            </table>
          </div>
          <div class="small-3 columns no-padding">
            <h1>Our Office </h1>
            <table class="tg2">
              <tr>
                <th class="tg-yw4l" colspan="3">Ruko Taman Kota Blok J1/7 , Bekasi Kota</th>
              </tr>
              <tr>
                <td class="tg-yw4l" colspan="3"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;Telp : (021) 883-56-330</td>
              </tr>
              <tr>
                <td class="tg-yw4l" colspan="3"><i class="fa fa-fax" aria-hidden="true"></i></i>&nbsp;&nbsp;Fax : (021) 883-56-007</td>
              </tr>
              <tr>
                <td class="tg-yw4l" colspan="3"><i class="fa fa-envelope-o" aria-hidden="true">&nbsp;&nbsp;Email : Mahkotadirfan@yahoo.co.id</td>
              </tr>
               <tr>
                <td class="tg-yw4l" colspan="3"><a href=""><p>Lihat Tentang Kami</p></a></td>
              </tr>
            </table>
          </div>
          <div class="small-3 columns no-padding">
            <h1>Social Media</h1>
            <table class="tg2">
              <tr>
                <th class="tg-yw4l" colspan="3"><i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp;&nbsp;Mahkotadirfanberkah</th>
              </tr>
              <tr>
                <td class="tg-yw4l" colspan="3"><i class="fa fa-twitter-square" aria-hidden="true"></i>&nbsp;&nbsp;@Mahkotadirfan</td>
              </tr>
              <tr>
                <td class="tg-yw4l" colspan="3"><i class="fa fa-linkedin-square" aria-hidden="true"></i>&nbsp;&nbsp;@Mahkotadirfan</td>
              </tr>
            </table>
          </div>
          <div class="small-3 columns no-padding">
          </div>
        </div>
      </div>
    </div>
    <div class="botfooter">
      <div class="row">
         <span>&copy;&nbsp;2011 - 2016  PT. Mahkota Dirfan Berkah</span>
      </div>
       <div class="upbutton">
        <a href="#" class="scrollToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
      </div>
    </div>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="js/jquery.polyglot.language.switcher.js" type="text/javascript"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script type="text/javascript">
          $(document).ready(function() {
           $('.flexslider').flexslider({
             animation: "fade",controlNav:true,directionNav:false, 
          });
          $('#polyglotLanguageSwitcher').polyglotLanguageSwitcher({
              effect: 'fade',
              testMode: true,
              onChange: function(evt){
                  alert("The selected language is: "+evt.selectedItem);
              }
          });
          $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
              $('.scrollToTop').fadeIn();
            } else {
              $('.scrollToTop').fadeOut();
            }
          });
        
          $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
          });
        });
    </script>
     
  </body>
</html>
