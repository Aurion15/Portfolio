<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	public function News(){
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->model('News_model');
		$this->load->library('session');
	}

	public function index(){
		$this->session->sess_destroy();
		$data['Latest'] = $this->News_model->latestNews();
		$data['News'] = $this->News_model->viewNews();
		$this->load->view('News', $data);
	}

	public function View(){
		if($this->session->userdata('log')){
			$data['News'] = $this->News_model->viewNews();
			$data['Edit'] = false;
			$data['message'] ='';
			$this->load->view('admin/News', $data);
		}else{
			redirect('MahkotaDirfan/index');
		}
	}

	public function insertNews(){
		$config['upload_path']   = './pictures/news/';
        $config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']      = 4000;
		$config['file_name'] = $this->input->post('title');
		$config['remove_spaces'] = FALSE;

		$this->load->library('upload', $config);
		 if ( ! $this->upload->do_upload('file')) {
		    $data['News'] = $this->News_model->viewNews();
			$data['Edit'] = false;
			$data['message'] ='please select a picture';
			$this->load->view('admin/News', $data);
		 }
		 else {
		 	$img_data=$this->upload->data();
            $new_imgname=$config['file_name'].$img_data['file_ext'];

			$newsData = array(
				'title'   => $this->input->post('title'),
				'note' => $this->input->post('note'),
				'picture' => $new_imgname
			);

			$this->News_model->insertNews($newsData);

		   redirect('News/View');
		 }
	}

	public function Detail($id){
		$data['News'] = $this->News_model->getNews($id);
		$this->load->view('Newsdetail', $data);
	}

	public function deleteNEws($id){
		if($this->session->userdata('log')){
			if(is_null($id)){
				redirect('/404/');
			}else{
				$Picture = $this->News_model->getPicture($id);
				$name = '';
				foreach ($Picture as $picture) {
					$name = $picture->picture;
				}

				$this->load->helper('file');
				unlink('./pictures/news/'.$name);

				$this->News_model->deleteNews($id);
				redirect('News/View');
			}
		}else{
			redirect('MahkotaDirfan/index');
		}
	}

	public function updateNews($id){
		if($this->session->userdata('log')){
			$data = array(
				'title'   => $this->input->post('title'),
				'note' => $this->input->post('note'),
	        );
			$this->News_model->updateNews($data, $id);
			$this->View();
		}else{
			redirect('MahkotaDirfan');
		}
	}

	public function ViewUpdate($id){
		if($this->session->userdata('log')){
			$data['News'] = $this->News_model->viewNews();
			$data['Edit'] = true;
			$data['id'] = $id;
			$data['Update'] = $this->News_model->getPicture($id);
			$this->load->view('admin/News', $data);
		}else{
			redirect('MahkotaDirfan');
		}
	}
}
?>