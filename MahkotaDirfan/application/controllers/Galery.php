<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galery extends CI_Controller {

	public function Galery(){
		parent::__construct();
		$this->load->helper(array('url','form'));
		$this->load->model('Galery_model');
		$this->load->library('session');
	}

	public function index(){
		$this->session->sess_destroy();
		$data['Galery'] = $this->Galery_model->viewGalery();
		$this->load->view('Galery', $data);
	}

	public function View(){
		if($this->session->userdata('log')){
			$data['Galery'] = $this->Galery_model->ViewGalery();
			$data['Edit'] = false;
			$data['message'] ='';
			$this->load->view('admin/Galery', $data);
		}else{
			redirect('MahkotaDirfan');
		}
	}

	public function Upload(){
		$config['upload_path']   = './pictures/galery/';
        $config['allowed_types'] = 'jpeg|jpg|png';
		$config['max_size']      = 4000;
		$config['remove_spaces'] = FALSE;

		$config['file_name'] = $this->input->post('pictureCaption');

		$this->load->library('upload', $config);
		 if ( ! $this->upload->do_upload('file')) {
		    $data['Galery'] = $this->Galery_model->ViewGalery();
			$data['Edit'] = false;
			$data['message'] ='please select a picture';
			$this->load->view('admin/Galery', $data);
		 }
		 else {
		 	$img_data=$this->upload->data();
            $new_imgname=$config['file_name'].$img_data['file_ext'];

			$pictureData = array(
				'caption' => $this->input->post('pictureCaption'),
				'picture' => $new_imgname
			);

			$this->Galery_model->uploadPicture($pictureData);

		   redirect('Galery/View');
		 }
	}

	public function deletePicture($id){
		if($this->session->userdata('log')){
			if(is_null($id)){
				redirect('/404/');
			}else{

				$Picture = $this->Galery_model->getPicture($id);
				$name = '';
				foreach ($Picture as $picture) {
					$name = $picture->picture;
				}

				$this->load->helper('file');
				unlink('./pictures/galery/'.$name);

				$this->Galery_model->deletePicture($id);
				redirect('Galery/View');
			}
		}else{
			redirect('MahkotaDirfan');
		}
	}

	public function updateGalery($id){
		if($this->session->userdata('log')){
			$data = array(
	            'caption'=> $this->input->post('pictureCaption'),
	        );
			$this->Galery_model->updateGalery($data, $id);
			$this->View();
		}else{
			redirect('MahkotaDirfan');
		}
	}

	public function ViewUpdate($id){
		if($this->session->userdata('log')){
			$data['Galery'] = $this->Galery_model->ViewGalery();
			$data['Edit'] = true;
			$data['id'] = $id;
			$data['Update'] = $this->Galery_model->getPicture($id);
			$this->load->view('admin/Galery', $data);
		}else{
			redirect('MahkotaDirfan');
		}
	}
}
?>