<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Category_model extends CI_Model {

		function __construct(){
			parent::__construct();
			$this->load->database();
		}


		function viewCategory(){
			$view = $this->db->get('category');
			return $view->result();
		}

		function getCategory($id){
			$view = $this->db->get_where('category',array('id'=>$id));
			return $view->result();
		}


		function addCategory($data){
			$this->db->insert('category',$data);
		}

		function deleteCategory($id){
			$this->db->delete('category',array('id'=>$id));
		}

		public function countRow(){
			$count = $this->db->query('SELECT COUNT(id) AS COUNT FROM category');
			return $count->result();
		}

		function updateCategory($data,$id){
			$this->db->where('id', $id);
			$this->db->update('category',$data);
		}
	}

?>