<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class User_model extends CI_Model {

		function __construct(){
			parent::__construct();
			$this->load->database();
		}

		function GetUser(){
			$d = $this->db->get('user');	
			return $d->result();
		}


	}

?>