<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/foundation/js/foundation.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/app.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.flexslider-min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.polyglot.language.switcher.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
           $('.flexslider').flexslider({
             animation: "fade",controlNav:false,directionNav:true, 
          });
          $('#polyglotLanguageSwitcher').polyglotLanguageSwitcher({
              effect: 'fade',
              testMode: true,
              onChange: function(evt){
                  alert("The selected language is: "+evt.selectedItem);
              }
          });
          $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
              $('.scrollToTop').fadeIn();
            } else {
              $('.scrollToTop').fadeOut();
            }
          });
        
          $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
          });
        });
    </script>
     <script type="text/javascript">
        $( function() {
          $( "#tabs" ).tabs();
        } );

        $('#tabs li').click(function(event){
          var id = $(event.target).index();
          $('.active').removeClass('active');
          $(event.target).addClass('active');
        });

         $(window).load(function() {
            $(".productcontent").mCustomScrollbar({
                   scrollInertia: 400
                });

             $(".newscontent").mCustomScrollbar({
                   scrollInertia: 400
            });
          }); 
  </script>