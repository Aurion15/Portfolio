<div class="footer">
      <div class="row">
        <div class="small-12 columns no-padding">
          <div class="small-3 columns no-padding">
            <h1>Jam Operasional </h1>
           <table class="tg">
              <tr>
                <th class="tg-yw4l">Senin-Jumat</th>
                <th class="tg-yw4l">     -     </th>
                <th class="tg-yw4l">08:30 - 16:30</th>
              </tr>
              <tr>
                <td class="tg-yw4l">Sabtu</td>
                <td class="tg-yw4l">     -     </td>
                <td class="tg-yw4l">08.30 - 13:30</td>
              </tr>
              <tr>
                <td class="tg-yw4l">Minggu /  Hari libur </td>
                <td class="tg-yw4l">     -     </td>
                <td class="tg-ljhg">Close</td>
              </tr>
            </table>
          </div>
          <div class="small-3 columns no-padding">
            <h1>Our Office </h1>
            <table class="tg2">
              <tr>
                <th class="tg-yw4l" colspan="3">Ruko Taman Kota Blok J1/7 , Bekasi Kota</th>
              </tr>
              <tr>
                <td class="tg-yw4l" colspan="3"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;Telp : (021) 883-56-330</td>
              </tr>
              <tr>
                <td class="tg-yw4l" colspan="3"><i class="fa fa-fax" aria-hidden="true"></i></i>&nbsp;&nbsp;Fax : (021) 883-56-007</td>
              </tr>
              <tr>
                <td class="tg-yw4l" colspan="3"><i class="fa fa-envelope-o" aria-hidden="true">&nbsp;&nbsp;Email : Mahkotadirfan@yahoo.co.id</td>
              </tr>
               <tr>
                <td class="tg-yw4l" colspan="3"><a href="<?php echo base_url().'About' ?>"><p>Lihat Tentang Kami</p></a></td>
              </tr>
            </table>
          </div>
          <div class="small-3 columns no-padding">
            <h1>Social Media</h1>
            <table class="tg2">
              <tr>
                <th class="tg-yw4l" colspan="3"><i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp;&nbsp;Mahkotadirfanberkah</th>
              </tr>
              <tr>
                <td class="tg-yw4l" colspan="3"><i class="fa fa-twitter-square" aria-hidden="true"></i>&nbsp;&nbsp;@Mahkotadirfan</td>
              </tr>
              <tr>
                <td class="tg-yw4l" colspan="3"><i class="fa fa-linkedin-square" aria-hidden="true"></i>&nbsp;&nbsp;@Mahkotadirfan</td>
              </tr>
            </table>
          </div>
          <div class="small-3 columns no-padding">
          </div>
        </div>
      </div>
    </div>
    <div class="botfooter">
      <div class="row">
         <span>&copy;&nbsp;2011 - 2016  PT. Mahkota Dirfan Berkah</span>
      </div>
       <div class="upbutton">
        <a href="#" class="scrollToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
      </div>
    </div>
    