<!doctype html>
<html class="no-js" lang="en">
  <head>
    <!-- Header  -->
    <?php $this->load->view('front/Headlib'); ?>
  </head>
  <body>
    <!-- Header  -->
    <?php $this->load->view('front/Header'); ?>

    <div class="sparatorhead">
      <div class="caption columns centered">
        <h1>OUR <span>GALLERY</span></h1>
        <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;/  </p>
      </div>
    </div>
    <div class="container no-padding">
      <div class="row">
        <div class="heading">
          <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp; GALERY
        </div>
        <div class="gallery">
          <div  class="large-12 columns no-padding">
          <div class="test-list">
             <ul class="list small-block-grid-4 ">
             <?php
                foreach ($Galery as $galery) { ?>
                  <li>
                    <div class="name">
                      <a href="#">
                        <img src="<?php echo base_url();?>pictures/galery/<?php echo $galery->picture?>" alt="">
                        <h3><?php echo $galery->caption?></h3>
                      </a>
                    </div>
                  </li>
             <?php   }
             ?>
            </ul>
            <ul class="pagination"></ul>
          </div>
          </div>
        </div>
        
      </div>
    </div>
    <div class="medpartner no-padding">
      <center><img src="<?php echo base_url();?>assets/img/sponsored.jpg" class="centered" alt=""></center>
    </div>

    <?php $this->load->view('front/Footer'); ?>
    
    <?php $this->load->view('front/Footlib'); ?>
     
  </body>
</html>
