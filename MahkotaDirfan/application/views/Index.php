<!doctype html>
<html class="no-js" lang="en">
  <head>
    
  <!-- Header  -->
  <?php $this->load->view('front/Headlib'); ?>
  
  </head>
  <body>

    <!-- Header  -->
    <?php $this->load->view('front/Header'); ?>

    <div class="flexslider">
      <ul class="slides">
        <li>
          <div class="mainslider" style="background-image:url(<?php echo base_url();?>assets/img/main_slider1ext.jpg);">
            <div class="row">
              <h1>Hospital, health Equipment Specialist</h1>
              <a href="">More</a>
            </div>
          </div>
        </li>
        <li>
          <div class="mainslider" style="background-image:url(<?php echo base_url();?>assets/img/main_slider1ext.jpg);">
            <div class="row">
              <h1>Hospital, health Equipment Specialist</h1>
              <a href="">More</a>
            </div>
          </div>
        </li>
        <li>
          <div class="mainslider" style="background-image:url(<?php echo base_url();?>assets/img/main_slider1ext.jpg);">
            <div class="row">
              <h1>Hospital, health Equipment Specialist</h1>
              <a href="">More</a>
            </div>
          </div>
        </li>

      </ul>
    </div>
    
    <div class="welcome">
        <div class="row ">
          <h1>WELCOME TO PT.MAHKOTA DIRFAN BERKAH</h1>
          <p>PT.Mahkota Dirfan Berkah adalah sebuah perusahaan distributor alat-alat kesehatan seperti disposible equipment, diagnostic tools, medical equiSpment dan alat-alat laboratorium lainya.  
          kami telah melayani ratusan rumah sakit dan instansi kesehatan di Indonesia. Kualitas, 
          kecepatan , komunikasi dan juga kepuasan pelanggan menjadi prioritas kami.
          </p>
        </div>
    </div>
    <div class="sliderbot  small-12 columns no-padding">
        <div class="panel small-5 columns no-padding"></div>
        <div id="tabs" class="caption small-7 columns no-padding">
              <ul class="tabs no-padding no-margin">
              <li class="active"><a href="#tabs-1">Quality &amp; Safety</a></li>
              <li><a href="#tabs-2">Costumer Support</a></li>
              <li><a href="#tabs-3">Avaible</a></li>
            </ul>
              <ul class="slides text">
                <li id="tabs-1">
                  <h1>Highest <span>Quality</span></h1>
                  <p>Our high quality standards reinforce your facility's caregiver and patient safety goals.</p>
                </li>
                <li id="tabs-2">
                <h1>Responsive <span>Customer Service</span></h1>
                  <p>24/7 service, delivered when and where you need it, in patient ready condition.</p>
                </li>
                <li id="tabs-3">
                <h1>Availability</h1>
                  <p>Access to over 400,000 pieces of medical and surgical equipment to meet your patient needs.</p>
                </li>
              </ul>
        </div>  
    </div>
    
    <div class="medic no-padding"">
      <div class="row">
        <ul class="small-block-grid-4">
    <?php
    $i = 0;
    foreach ($Category as $category) {
      if($i < 4){ ?>
        <li>
            <div class="option">
              <img src="<?php echo base_url();?>pictures/category/<?php echo $category->picture?>" alt="">
              <h1><?php echo $category->name; ?></h1>
              <a href="<?php echo base_url().'Product/Category/'.$category->id?>">Load more</a>
            </div>
        </li>
    <?php  }
    $i++;}
    ?>
        </ul>
      </div>
    </div>

    <div class="feature">
      <div class="row">
        <div class="featrow small-12 columns no-padding">
          <div class="galery small-4 columns no-padding">
            <h3>Our</h3>
            <h2>Galery</h2>
            <div class="galerycontent small-12 columns no-padding">
              <div class="flexslider">
                <ul class="slides">
                <?php 
                  foreach ($Galery as $galery) { ?>
                    <li>
                      <img src="<?php echo base_url();?>pictures/galery/<?php echo $galery->picture ;?>" />
                    </li>
                <?php  }
                ?>
                </ul>
              </div>
            </div>
          </div>
          <div class="news small-4 columns no-padding">
            <h3>LATEST</h3>
            <h2>News</h2>
            <div class="newscontent mCustomScrollbar small-12 columns no-padding" data-mcs-theme="minimal-dark">
                <ul>
                <?php 
                      foreach ($News as $news) { 
                      $originalDate = $news->date;
                      $newDate = date("d-M-Y", strtotime($originalDate));?>
                        <li>
                          <a href="<?php echo base_url().'News/Detail/'.$news->id?>">
                            <h4><?php echo $newDate; ?></h4>
                            <h3><?php echo $news->title; ?></h3>
                            <p><?php echo $news->note; ?></p>
                          </a>
                        </li>
                    <?php  }
                    ?>
                </ul>
            </div>
          </div>
          <div class="product small-4 columns no-padding">
            <h3>latest</h3>
            <h2>PRODUCT</h2>
            <div class="productcontent mCustomScrollbar small-12 columns no-padding"  data-mcs-theme="minimal-dark">
              <ul>
                <?php  
                  foreach ($Product as $product) { ?>
                    <li>
                      <a href="<?php echo base_url().'Product/Detail/'.$product->id?>">
                      <img src="<?php echo base_url();?>pictures/product/<?php echo $product->picture ;?>" alt="">
                      <div class="caption">
                        <h3><?php echo $product->name ;?></h3>
                        <p><?php echo $product->note ;?></p>
                      </div>
                      </a>
                    </li>
                <?php  }
                ?>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('front/Footer'); ?>
    
    <?php $this->load->view('front/Footlib'); ?>
    
  </body>
</html>
