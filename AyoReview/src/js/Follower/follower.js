class Follower {
  constructor(){
  }
  async getFollower(user){
    const followerResponse = await fetch('follower.json');
    const follower = followerResponse.json();
    return {
      follower
    }
  }
}