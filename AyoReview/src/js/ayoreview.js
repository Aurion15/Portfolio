 /*
          ___   ____    ____  ______   .______       ___________    ____  __   ___________    __    ____ 
         /   \  \   \  /   / /  __  \  |   _  \     |   ____\   \  /   / |  | |   ____\   \  /  \  /   / 
        /  ^  \  \   \/   / |  |  |  | |  |_)  |    |  |__   \   \/   /  |  | |  |__   \   \/    \/   /  
       /  /_\  \  \_    _/  |  |  |  | |      /     |   __|   \      /   |  | |   __|   \            /   
      /  _____  \   |  |    |  `--'  | |  |\  \----.|  |____   \    /    |  | |  |____   \    /\    /    
     /__/     \__\  |__|     \______/  | _| `._____||_______|   \__/     |__| |_______|   \__/  \__/   


       	Version: 0.1
     	  Author:  Agum Junianto
      	  Website: http://Ayoreview.com

      */
 // Fungsi global 

 function loaded() {
   setTimeout(function () {
     document.getElementById("loader").style.display = "none";
     document.getElementById("kontent").style.visibility = "visible";
   }, 10);
 }

 // fungsi circle rate
 $(document).ready(function () {
   $('.progress .progress-bar').css("width",
     function () {
       return $(this).attr("aria-valuenow") + "%";
     }
   );
 });
 // end fungsi circle rate


 // end  Fungsi global

 if ((top.location.pathname !== '/') || (top.location.pathname !== '/index.html') || (top.location.pathname !== '/profile_review.html') || (top.location.pathname !== '/profile_follower.html') || (top.location.pathname !== '/profile_media.html') || (top.location.pathname !== '/profile_following.html')) {
   // Slick js untuk top news 
   $(document).ready(function () {
     $('.top-news-content').slick({
       dots: false,
       infinite: true,
       speed: 1000,
       autoplaySpeed: 7000,
       slidesToShow: 1,
       adaptiveHeight: true,
       autoplay: true,
       arrows: false
     });
   });
   // end Slick js untuk top news
 }

 // FUNGSI PADA MENU INDEX

 if ((top.location.pathname === '/') || (top.location.pathname === '/index.html')) {
   $(window).scroll(function () {
     if ($('.nav-index').offset().top > 50) {
       $('.fixed-top').removeClass('navbar-dark py-3');
       $('.fixed-top').addClass('py-1 top-nav-collapse navbar-light top-nav-collapse');
       $('.navbar-brand').addClass('navbar-brand-scroll');
       $('.login-border').css({
         'color': 'rgba(48, 76, 84, 1)',
         'border': 'solid 1px rgba(48, 76, 84, 1)',
       });
       $('.kategori-border').css({
         'color': 'rgba(48, 76, 84, 1)',
         'border': 'solid 1px rgba(48, 76, 84, 1)',
       });
     } else {
       $('.fixed-top').removeClass('py-1 top-nav-collapse navbar-light top-nav-collapse');
       $('.fixed-top').addClass('navbar-dark py-3');
       $('.navbar-brand').removeClass('navbar-brand-scroll');
       $('.login-border').css({
         'color': 'rgba(255, 255, 255,0.9)',
         'border': 'solid 1px rgba(255, 255, 255,0.9)',
       });
       $('.kategori-border').css({
         'color': '#fff',
         'border': 'solid 1px rgba(255, 255, 255,0.9)',
       });
     }
   });
 } // end navigasi menu index
 // END FUNGSI PADA MENU INDEX













 //JAVASCRIPT PADA HALAMAN FULL-NEWS

 if ((top.location.pathname === '/news/news_full.html')) {
   // Untuk memunculkan kolom disqus
   //  const disqusUI = document.querySelector('.disqus');
   //  const komentar = document.getElementById('showcomment');
   //  disqusUI.style.display= "none";
   //  komentar.addEventListener('click', function () {
   //    disqusUI.style.display = "block";
   //    komentar.style.display = "none";
   //  }); 
   // End memunculkan Kolon disqus
 }

 // End JAVASCRIPT PADA HALAMAN FULL-NEWS


 if ((top.location.pathname === '/news/news.html')) {

 }











 // JAVASCRIPT Pada Halaman FULLREVIEW
 if ((top.location.pathname === '/fullreview.html')) {


   // Fungsi Readmore 
   $(document).ready(function () {
     var readMoreHtml = $(".read-more").html();

     var lessText = readMoreHtml.substr(0, 650);
     if (readMoreHtml.length > 100) {
       $(".read-more").html(lessText);
     } else {
       $(".read-more").html(readMoreHtml);
     }
   }); // end fungsi read-more


   // Fungsi Hide and show untuk konten Review Pada halaman fullreview
   $(document).ready(function () {
     $("#review").hide();
     $(".second").click(function () {
       $('html, body').animate({
         scrollTop: $(".tab-content").offset().top
       }, 500);
       $("#review").show();
       $("#second").show();
       $("#disqus-comment").hide();
     });
   });

   $(document).ready(function () {
     $(".hide").click(function () {
       $('html, body').animate({
         scrollTop: $("main").offset().top
       }, 500);
       $("#review").hide();
       $("#second").hide();
       $("#disqus-comment").show();
     });
   });
   // end Fungsi Hide and show untuk konten Review Pada halaman fullreview

   // Slider menggunakan slick slider pada halaman fullreview 
   $(document).ready(function () {
     $('.review-slider').slick({
       slidesToShow: 1,
       slidesToScroll: 1,
       arrows: false,
       fade: true,
       infinite: false,
       adaptiveHeight: true,
       cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
       asNavFor: '.slider-nav'
     });

     $('.slider-nav').slick({
       slidesToShow: 5,
       slidesToScroll: 1,
       asNavFor: '.review-slider',
       dots: false,
       focusOnSelect: true,
       infinite: false,
       arrows: true,
     });

     $('.slider-nav img').click(function () {
       $('img').removeClass('active');
       $(this).addClass("active");
     });
   }); // End fungsi slider

   // Fungsi tab wizard

   $('.tab-link').on('click', function (event) {
     // Prevent url change
     event.preventDefault();
     $('#pills-pro-tab').removeClass('not-actived');
     $('#pills-pro-tab').addClass('wizard-actived');
     // `this` is the clicked <a> tag
     var target = $('[data-toggle="tab"][href="' + this.hash + '"]');
     // opening tab
     target.trigger('click');
     // scrolling into view
     target[0].scrollIntoView(true);
     $(this).hide();
     $('.tab-link2').show();
     $('.tab-link-prev2').show();
   });
   $('.tab-link-prev2').on('click', function (event) {
     // Prevent url change
     event.preventDefault();

     // `this` is the clicked <a> tag
     var target = $('[data-toggle="tab"][href="' + this.hash + '"]');
     // opening tab
     target.trigger('click');
     // scrolling into view
     target[0].scrollIntoView(true);
     $(this).hide();
     $('.tab-link2').hide();
     $('.tab-link-prev2').hide();
     $('.tab-link').show();
   });
   $('.tab-link2').on('click', function (event) {
     // Prevent url change
     event.preventDefault();
     $('#pills-cons-tab').removeClass('not-actived');
     $('#pills-cons-tab').addClass('wizard-actived');
     // `this` is the clicked <a> tag
     var target = $('[data-toggle="tab"][href="' + this.hash + '"]');
     // opening tab
     target.trigger('click');
     // scrolling into view
     target[0].scrollIntoView(true);
     $(this).hide();
     $('.tab-link-prev2').hide();
     $('.tab-link3').show();
     $('.tab-link-prev3').show();
   });
   $('.tab-link-prev3').on('click', function (event) {
     // Prevent url change
     event.preventDefault();

     // `this` is the clicked <a> tag
     var target = $('[data-toggle="tab"][href="' + this.hash + '"]');
     // opening tab
     target.trigger('click');
     // scrolling into view
     target[0].scrollIntoView(true);
     $(this).hide();
     $('.tab-link3').hide();
     $('.tab-link-prev3').hide();
     $('.tab-link-prev2').show();
     $('.tab-link2').show();
   });

   $('.tab-link3').on('click', function (event) {
     // Prevent url change
     event.preventDefault();
     $('#pills-rec-tab').removeClass('not-actived');
     $('#pills-rec-tab').addClass('wizard-actived');
     // `this` is the clicked <a> tag
     var target = $('[data-toggle="tab"][href="' + this.hash + '"]');
     // opening tab
     target.trigger('click');
     // scrolling into view
     target[0].scrollIntoView(true);
     $(this).hide();
     $('.tab-link-prev3').hide();
     $('.tab-link4').show();
     $('.tab-link-prev4').show();
   });

   $('.tab-link-prev4').on('click', function (event) {
     // Prevent url change
     event.preventDefault();

     // `this` is the clicked <a> tag
     var target = $('[data-toggle="tab"][href="' + this.hash + '"]');
     // opening tab
     target.trigger('click');
     // scrolling into view
     target[0].scrollIntoView(true);
     $(this).hide();
     $('.tab-link4').hide();
     $('.tab-link-prev4').hide();
     $('.tab-link-prev3').show();
     $('.tab-link3').show();
   });

   $('#pills-home-tab').on('click', function (event) {
     // Prevent url change
     event.preventDefault();
     $('.tab-link').show();
     $('.tab-link2').hide();
     $('.tab-link3').hide();
     $('.tab-link4').hide();
     $('.tab-link-prev2').hide();
     $('.tab-link-prev3').hide();
     $('.tab-link-prev4').hide();
   });

   $('#pills-pro-tab').on('click', function (event) {
     // Prevent url change
     event.preventDefault();
     $('.tab-link').hide();
     $('.tab-link2').show();
     $('.tab-link3').hide();
     $('.tab-link4').hide();
     $('.tab-link-prev2').show();
     $('.tab-link-prev3').hide();
     $('.tab-link-prev4').hide();
   });

   $('#pills-cons-tab').on('click', function (event) {
     // Prevent url change
     event.preventDefault();
     $('.tab-link').hide();
     $('.tab-link2').hide();
     $('.tab-link3').show();
     $('.tab-link4').hide();
     $('.tab-link-prev2').hide();
     $('.tab-link-prev3').show();
     $('.tab-link-prev4').hide();
   });
   $('#pills-rec-tab').on('click', function (event) {
     // Prevent url change
     event.preventDefault();
     $('.tab-link').hide();
     $('.tab-link2').hide();
     $('.tab-link3').hide();
     $('.tab-link4').show();
     $('.tab-link-prev2').hide();
     $('.tab-link-prev3').hide();
     $('.tab-link-prev4').show();
   });

   $('#btnRec').on('click', function (e) {
     e.preventDefault();
     $(this).removeClass('btn-outline-success');
     $(this).addClass('btn-success');
     $('#btnUnrec').addClass('btn-outline-danger');
     $('#btnUnrec').removeClass('btn-danger');
   });

   $('#btnUnrec').on('click', function (e) {
     e.preventDefault();
     $(this).removeClass('btn-outline-danger');
     $(this).addClass('btn-danger');
     $('#btnRec').addClass('btn-outline-success');
     $('#btnRec').removeClass('btn-success');
   });







   // Fungsi upload image pada wizard fullreview

   //Check File API support
   if (window.File && window.FileList && window.FileReader) {
     var filesInput = document.getElementById("fileInput");

     filesInput.addEventListener("change", function (event) {

       var files = event.target.files; //FileList object
       var output = document.getElementById("fileResult");

       for (var i = 0; i < files.length; i++) {
         var file = files[i];

         //Only pics
         if (!file.type.match('image'))
           continue;

         var picReader = new FileReader();

         picReader.addEventListener("load", function (event) {

           var picFile = event.target;

           var div = document.createElement("div");
           div.style.display = "inline-block";

           div.innerHTML = "<img class='thumbnail' height='100px' width='100x' src='" + picFile.result + "'" +
             "title='" + picFile.name + "'/>";

           output.insertBefore(div, null);

         });

         //Read the image
         picReader.readAsDataURL(file);
       }

     });
   } else {
     console.log("Your browser does not support File API");
   }
   // end Fungsi upload image pada wizard fullreview

   // Create new PRO object
   class Pro {
     constructor(proDetail) {
       this.proDetail = proDetail;
     }
   }

   class Cons {
     constructor(consDetail) {
       this.consDetail = consDetail;
     }
   }

   class UI {
     addProToList(pro) {
       const list = document.getElementById('proResults');
       // Create tr Element
       const row = document.createElement('ul');
       row.className = 'list-group';
       // Insert cols
       row.innerHTML = `
          <li class="list-group-item mt-2">${pro.proDetail} <a href="" class="delete pull-right fa fa-remove"></a></li>
        `;
       list.appendChild(row);
     }

     addConsToList(cons) {
       const list = document.getElementById('consResults');
       // Create tr Element
       const row = document.createElement('ul');
       row.className = 'list-group';
       // Insert cols
       row.innerHTML = `
          <li class="list-group-item mt-2">${cons.consDetail} <a href="" class="delete pull-right fa fa-remove"></a></li>
        `;
       list.appendChild(row);
     }
     deletePro(target) {
       if (target.className === 'delete pull-right fa fa-remove') {
         target.parentElement.parentElement.remove();
       }
     }

     deleteCons(target) {
       if (target.className === 'delete pull-right fa fa-remove') {
         target.parentElement.parentElement.remove();
       }
     }
     clearFields() {
       document.getElementById('keunggulan').value = '';
     }
   }

   class Storage {
     // Get Book from storage
     static getPros() {
       let pros;
       if (localStorage.getItem('pros') === null) {
         pros = [];
       } else {
         pros = JSON.parse(localStorage.getItem('pros'));
       }
       return pros;
     }

     // Display book from Storage
     static displayPros() {
       const pros = Storage.getPros();
       pros.forEach(function (pro) {
         const ui = new UI();
         // Add book to UI
         ui.addProToList(pro);
       });
     }

     // Add book from storage
     static addPros(pro) {
       const pros = Storage.getPros();
       pros.push(pro);
       localStorage.setItem('pros', JSON.stringify(pros));
     }

     static removePros(proDetail) {
       const pros = Storage.getPros();
       pros.forEach(function (pro, index) {
         if (pro.proDetail === proDetail) {
           pros.splice(index, 1);
         }
       });

       localStorage.setItem('pros', JSON.stringify(pros));
     }
   }

   // DOM load Event
   document.addEventListener('DOMContentLoaded', Storage.displayPros);

   // event listener on click
   document.getElementById('formPro').addEventListener('submit', function (e) {
     e.preventDefault();
     // Get form values
     const proDetail = document.getElementById('keunggulan').value;

     // Instantiate new pro
     const pro = new Pro(proDetail);

     // Instantiate UI
     const ui = new UI();

     // Validate
     if (proDetail === '') {
       alert('Error Form tidak boleh Kosong');
     } else {
       // add pro to list
       ui.addProToList(pro);
       // Add to local Storage
       Storage.addPros(pro);
       // Clear Field
       ui.clearFields();
     }
   });

   // event listener on click
   document.getElementById('formCons').addEventListener('submit', function (e) {
     e.preventDefault();
     // Get form values
     const consDetail = document.getElementById('kekurangan').value;

     // Instantiate new pro
     const cons = new Cons(consDetail);

     // Instantiate UI
     const ui = new UI();

     // Validate
     if (consDetail === '') {
       alert('Error Form tidak boleh Kosong');
     } else {
       // add pro to list
       ui.addConsToList(cons);
       // Add to local Storage
       // Storage.addPros(pro);
       // Clear Field
       ui.clearFields();
     }
   });


   document.getElementById('proResults').addEventListener('click', function (e) {
     e.preventDefault();
     const ui = new UI();
     ui.deletePro(e.target);
     Storage.removePros(e.target.parentElement.textContent);
   });

   document.getElementById('consResults').addEventListener('click', function (e) {
     e.preventDefault();
     const ui = new UI();
     ui.deleteCons(e.target);
   });
   document.getElementById('btnRec').addEventListener('click', function (e) {
     e.preventDefault();
     let result = document.getElementById('recResults');
     result.innerHTML = `
        <div class="text-center">
          <h3 style = "background-color: #e4f4fa;
          width: 85 % ;
          border: solid 1 px #3fc4f5;" class="py-4">Saya Merekomendasikan Ini</h3>
        </div>
      `;
   });

   document.getElementById('btnUnrec').addEventListener('click', function (e) {
     e.preventDefault();
     let result = document.getElementById('recResults');
     result.innerHTML = `
        <div class="text-center">
          <h3 style = " background-color: #f7d9d5;
          width: 85 % ;
          border: solid 1 px# F45944;" class="py-4">Saya Tidak Merekomendasikan Ini</h3>
        </div>
      `;
   });

 }
 // end JAVASCRIPT Pada Halaman FULLREVIEW


 // Javascript pada halaman profile follower
 if ((top.location.pathname === '/profile_follower.html')) {
   let userFollower = document.querySelectorAll('.user-follower');

   userFollower.forEach(function (item, index) {
     console.log(item.childElementCount);
   });
 }

 // Javascript untuk halaman profile_review
 if ((top.location.pathname === '/profile_review.html')) {
   class Follow {
     constructor(id, idTarget) {
       this.id = id;
       this.idTarget = idTarget;
     }
   }

   class Storage {
     // Get following from storage
     static getFollows() {
       let follows;
       if (localStorage.getItem('follows') === null) {
         follows = [];
       } else {
         follows = JSON.parse(localStorage.getItem('follows'));
       }

       return follows;
     }

     static displayFollows() {
       const follows = Storage.getFollows();
       follows.forEach(function (follow, index) {
         if (follow.id !== null) {
           $("#follow").addClass('following');
           $("#follow").attr('data-following', 'true');
         }
       });
     }

     static setFollowing(follow) {
       const follows = Storage.getFollows();
       follows.push(follow);
       localStorage.setItem('follows', JSON.stringify(follows));
     }

     static setUnfollowing(idTarget) {
       const follows = Storage.getFollows();
       follows.forEach(function (follow, index) {
         if (follow.idTarget === idTarget) {
           follows.splice(index, 1);
         }
       });
       localStorage.setItem('follows', JSON.stringify(follows));
     }
   }

   // Load follow state
   document.addEventListener('DOMContentLoaded', Storage.displayFollows);

   $("button").click(function (e) {
     if ($(this).attr('data-following') === 'false') {
       // Create Object untuk di push ke storage
       const id = 1; // Id user yang akan di follow
       const idTarget = 2; // Status follow users
       const follow = new Follow(id, idTarget);
       // Push follow ke Local Storage
       Storage.setFollowing(follow);
       $(this).attr('data-following', 'true');
       $(this).addClass('following');
       if ($(this).is('.following')) {
         $(this).addClass('wait');
       }
     } else if ($(this).attr('data-following') === 'true') {
       // Create object untuk di unFollow
       const idTarget = 2; // Id user yang akan di unfollow
       Storage.setUnfollowing(idTarget);
       $(this).attr('data-following', 'false');
       $(this).removeClass('following');
     }
     e.preventDefault();
   });

   // End follow button
 }




 // FUNGSI PADA KATEGORI PRODUK UNTUK DIREVIEW
 if ((top.location.pathname === '/audiophile.html') || (top.location.pathname === '/jamtangan.html')) {
   new AnimOnScroll(document.getElementById('grid'), {
     minDuration: 0.4,
     maxDuration: 0.7,
     viewportFactor: 0.2
   });
   $(document).ready(function () {
     $('.reset-select').hide();

     $('.sub-catagories').select2({
       placeholder: "SUB KATEGORI",
     });
     $('.brand').select2({
       placeholder: "BRAND",
     });
     $('.price').select2({
       placeholder: "HARGA",
     });
     $('.sort-review').select2({
       placeholder: "URUTKAN",
     });
     $('.sub-catagories, .brand, .price, .sort-review').on('change', function () {
       $('.reset-select').show();
     });
     $('.reset-select').click(function () {
       $('.sub-catagories, .brand, .price, .sort-review').val(null).trigger('change');
       $('.reset-select').hide();
     });
   });
 }

 // END FUNGSI PADA KATEGORI PRODUK UNTUK DIREVIEW

 // Search morph
 (function () {
   var morphSearch = document.getElementById('morphsearch'),
     bigSearch = document.getElementById('BigSearch'),
     hideSearch = document.getElementById('hideSearch'),
     input = morphSearch.querySelector('input.morphsearch-input'),
     ctrlClose = morphSearch.querySelector('span.morphsearch-close'),
     navbar = document.querySelector('.navbar');
   isOpen = isAnimating = false,

     // show/hide search area
     toggleSearch = function (evt) {
       // Show/hide li search in index
       if ((top.location.pathname === '/') || (top.location.pathname === '/index.html')) {
         hideSearch.classList.remove('unreveal');
       }
       // return if open and the input gets focused
       if (evt.type.toLowerCase() === 'focus' && isOpen) return false;
       var offsets = morphsearch.getBoundingClientRect();
       if (isOpen) {
         $('body').removeClass('stop-scrolling');
         navbar.classList.remove("nav-full");
         classie.remove(morphSearch, 'open');
         // trick to hide input text once the search overlay closes 
         // todo: hardcoded times, should be done after transition ends
         if (input.value !== '') {
           setTimeout(function () {
             classie.add(morphSearch, 'hideInput');
             setTimeout(function () {
               classie.remove(morphSearch, 'hideInput');
               input.value = '';
             }, 300);
           }, 500);
         }
         input.blur();
         if ((top.location.pathname === '/') || (top.location.pathname === '/index.html')) {
           setTimeout(function () {
             hideSearch.classList.add('unreveal');
           }, 300);
         }
       } else {
         $('body').addClass('stop-scrolling');
         classie.add(morphSearch, 'open');
         setTimeout(function () {
           navbar.classList.add('nav-full');
         }, 400);
       }
       isOpen = !isOpen;
     };

   // events
   if ((top.location.pathname === '/') || (top.location.pathname === '/index.html')) {
     bigSearch.addEventListener('focus', toggleSearch);
   }

   input.addEventListener('focus', toggleSearch);
   ctrlClose.addEventListener('click', toggleSearch);

   // esc key closes search overlay
   // keyboard navigation events
   document.addEventListener('keydown', function (ev) {
     var keyCode = ev.keyCode || ev.which;
     if (keyCode === 27 && isOpen) {
       toggleSearch(ev);
     }
   });
   /***** for demo purposes only: don't allow to submit the form *****/
   morphSearch.querySelector('button[type="submit"]').addEventListener('click', function (ev) {
     ev.preventDefault();
   });
 })();

 $(document).on("keypress", "form", function (event) {
   return event.keyCode != 13;
 });