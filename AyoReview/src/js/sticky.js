

/* buat ngatur sticki sidebar */
 if ((top.location.pathname === '/fullreview.html') || (top.location.pathname === '/review_pengguna.html') || (top.location.pathname === '/edit-review.html')) {
  $(document).ready(function(){
    $('main.container .col-lg-3').theiaStickySidebar({
        // Settings
        additionalMarginTop: 70
      });
  });
 }else if((top.location.pathname === '/profile_review.html') || (top.location.pathname === '/profile_media.html') || (top.location.pathname === '/profile_follower.html') || (top.location.pathname === '/profile_following.html') || (top.location.pathname == '/profile_edit.html')){
   $('main.container .col-lg-3').theiaStickySidebar({
        // Settings
        additionalMarginTop: 120
      });
 }



$(document).ready(function() {
  // Custom 
  var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
    var stickyHeight = sticky.outerHeight();
    var stickyTop = stickyWrapper.offset().top - 60;
    if (scrollElement.scrollTop() >= stickyTop){
      stickyWrapper.height(stickyHeight);
      sticky.addClass("is-sticky");
      if (top.location.pathname === '/fullreview.html'){
        $(".is-sticky").css("padding-left", "8%");
      }
    }
    else{
      if (top.location.pathname === '/fullreview.html'){
        $(".is-sticky").css("padding-left", "");
      }

      sticky.removeClass("is-sticky");
      stickyWrapper.height('auto'); 
    }
  };
  


  // Find all data-toggle="sticky-onscroll" elements
  $('[data-toggle="sticky-onscroll"]').each(function() {
    var sticky = $(this);
    var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
    sticky.before(stickyWrapper);
    sticky.addClass('sticky');
    
    // Scroll & resize events
    $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
      stickyToggle(sticky, stickyWrapper, $(this));
    });
    
    // On page load
    stickyToggle(sticky, stickyWrapper, $(window));
  });
});